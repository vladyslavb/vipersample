//
//  ListStoryAssembly.m
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "ListStoryAssembly.h"

// Frameworks
#import <ViperMcFlurry/ViperMcFlurry.h>

// Classes
#import "ListStoryViewController.h"
#import "ListStoryInteractor.h"
#import "ListStoryPresenter.h"
#import "ListStoryRouter.h"


@implementation ListStoryAssembly


#pragma mark - Initialization methods -

- (ListStoryViewController*) viewListStory 
{
    return [TyphoonDefinition withClass: [ListStoryViewController class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterListStory]];
                              [definition injectProperty: @selector(moduleInput)
                                                    with: [self presenterListStory]];
                                                    
                          }];
}

- (ListStoryInteractor*) interactorListStory 
{
    return [TyphoonDefinition withClass: [ListStoryInteractor class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterListStory]];
                                                    
                          }];
}

- (ListStoryPresenter*) presenterListStory
{
    return [TyphoonDefinition withClass: [ListStoryPresenter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(view)
                                                    with: [self viewListStory]];
                              [definition injectProperty: @selector(interactor)
                                                    with: [self interactorListStory]];
                              [definition injectProperty: @selector(router)
                                                    with: [self routerListStory]];
                                                    
                          }];
}

- (ListStoryRouter*) routerListStory
{
    return [TyphoonDefinition withClass: [ListStoryRouter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(transitionHandler)
                                                    with: [self viewListStory]];
                                                    
                          }];
}

@end
