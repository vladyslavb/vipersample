//
//  DetailStoryAssembly.m
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "DetailStoryAssembly.h"

// Frameworks
#import <ViperMcFlurry/ViperMcFlurry.h>

// Classes
#import "DetailStoryViewController.h"
#import "DetailStoryInteractor.h"
#import "DetailStoryPresenter.h"
#import "DetailStoryRouter.h"


@implementation DetailStoryAssembly


#pragma mark - Initialization methods -

- (DetailStoryViewController*) viewDetailStory 
{
    return [TyphoonDefinition withClass: [DetailStoryViewController class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterDetailStory]];
                              [definition injectProperty: @selector(moduleInput)
                                                    with: [self presenterDetailStory]];
                                                    
                          }];
}

- (DetailStoryInteractor*) interactorDetailStory 
{
    return [TyphoonDefinition withClass: [DetailStoryInteractor class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterDetailStory]];
                                                    
                          }];
}

- (DetailStoryPresenter*) presenterDetailStory
{
    return [TyphoonDefinition withClass: [DetailStoryPresenter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(view)
                                                    with: [self viewDetailStory]];
                              [definition injectProperty: @selector(interactor)
                                                    with: [self interactorDetailStory]];
                              [definition injectProperty: @selector(router)
                                                    with: [self routerDetailStory]];
                                                    
                          }];
}

- (DetailStoryRouter*) routerDetailStory
{
    return [TyphoonDefinition withClass: [DetailStoryRouter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(transitionHandler)
                                                    with: [self viewDetailStory]];
                                                    
                          }];
}

@end
