//
//  ListStoryViewController.h
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
@import UIKit;

// Classes
#import "ListStoryViewInput.h"

// Protocols
@protocol ListStoryViewOutput;

@interface ListStoryViewController : UIViewController <ListStoryViewInput>

@property (nonatomic, strong) id<ListStoryViewOutput> output;

@end
