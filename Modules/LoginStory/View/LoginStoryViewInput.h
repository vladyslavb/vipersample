//
//  LoginStoryViewInput.h
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
@import Foundation;

// Protocols
@protocol LoginStoryViewInput <NSObject>

/**
 @author Vladyslav Bedro

 Method for setup initial state of view
 */
- (void) setupInitialState;

@end
