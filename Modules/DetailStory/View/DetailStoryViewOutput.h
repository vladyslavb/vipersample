//
//  DetailStoryViewOutput.h
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
@import Foundation;

// Protocols
@protocol DetailStoryViewOutput <NSObject>

/**
 @author Vladyslav Bedro
 
 Method which inform presenter, that view is ready for a work
 */
- (void) didTriggerViewReadyEvent;

@end
