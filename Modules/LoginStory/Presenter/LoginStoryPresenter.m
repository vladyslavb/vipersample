//
//  LoginStoryPresenter.m
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "LoginStoryPresenter.h"

// Classes
#import "LoginStoryViewInput.h"
#import "LoginStoryInteractorInput.h"
#import "LoginStoryRouterInput.h"

@implementation LoginStoryPresenter


#pragma mark - Methods LoginStoryModuleInput -

- (void) configureModule 
{
	/**
	@author Vladyslav Bedro
	
	Starting configuration of the module, which dependent to the view state
	*/
}


#pragma mark - Methods LoginStoryViewOutput -

- (void) didTriggerViewReadyEvent 
{
	[self.view setupInitialState];
}


#pragma mark - Methods LoginStoryInteractorOutput -


@end
