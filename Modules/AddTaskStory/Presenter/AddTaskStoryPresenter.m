//
//  AddTaskStoryPresenter.m
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "AddTaskStoryPresenter.h"

// Classes
#import "AddTaskStoryViewInput.h"
#import "AddTaskStoryInteractorInput.h"
#import "AddTaskStoryRouterInput.h"

@implementation AddTaskStoryPresenter


#pragma mark - Methods AddTaskStoryModuleInput -

- (void) configureModule 
{
	/**
	@author Vladyslav Bedro
	
	Starting configuration of the module, which dependent to the view state
	*/
}


#pragma mark - Methods AddTaskStoryViewOutput -

- (void) didTriggerViewReadyEvent 
{
	[self.view setupInitialState];
}


#pragma mark - Methods AddTaskStoryInteractorOutput -


@end
