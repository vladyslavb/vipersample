//
//  DetailStoryRouter.h
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "DetailStoryRouterInput.h"

// Protocols
@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface DetailStoryRouter : NSObject <DetailStoryRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
