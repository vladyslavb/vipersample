//
//  ListStoryPresenter.h
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "ListStoryViewOutput.h"
#import "ListStoryInteractorOutput.h"
#import "ListStoryModuleInput.h"

// Protocols
@protocol ListStoryViewInput;
@protocol ListStoryInteractorInput;
@protocol ListStoryRouterInput;

@interface ListStoryPresenter : NSObject <ListStoryModuleInput, ListStoryViewOutput, ListStoryInteractorOutput>

@property (nonatomic, weak) id<ListStoryViewInput> view;
@property (nonatomic, strong) id<ListStoryInteractorInput> interactor;
@property (nonatomic, strong) id<ListStoryRouterInput> router;

@end
