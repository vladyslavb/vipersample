//
//  AddTaskStoryInteractorInput.h
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
@import Foundation;

@protocol AddTaskStoryInteractorInput <NSObject>

@end
