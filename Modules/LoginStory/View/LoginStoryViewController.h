//
//  LoginStoryViewController.h
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
@import UIKit;

// Classes
#import "LoginStoryViewInput.h"

// Protocols
@protocol LoginStoryViewOutput;

@interface LoginStoryViewController : UIViewController <LoginStoryViewInput>

@property (nonatomic, strong) id<LoginStoryViewOutput> output;

@end
