//
//  DetailStoryAssembly.h
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

/**
 @author Vladyslav Bedro

 DetailStory module
 */
@interface DetailStoryAssembly : TyphoonAssembly <RamblerInitialAssembly>

@end
