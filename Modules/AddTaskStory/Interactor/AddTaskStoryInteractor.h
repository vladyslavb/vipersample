//
//  AddTaskStoryInteractor.h
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "AddTaskStoryInteractorInput.h"

// Protocols
@protocol AddTaskStoryInteractorOutput;

@interface AddTaskStoryInteractor : NSObject <AddTaskStoryInteractorInput>

@property (nonatomic, weak) id<AddTaskStoryInteractorOutput> output;

@end
