//
//  DetailStoryPresenter.h
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "DetailStoryViewOutput.h"
#import "DetailStoryInteractorOutput.h"
#import "DetailStoryModuleInput.h"

// Protocols
@protocol DetailStoryViewInput;
@protocol DetailStoryInteractorInput;
@protocol DetailStoryRouterInput;

@interface DetailStoryPresenter : NSObject <DetailStoryModuleInput, DetailStoryViewOutput, DetailStoryInteractorOutput>

@property (nonatomic, weak) id<DetailStoryViewInput> view;
@property (nonatomic, strong) id<DetailStoryInteractorInput> interactor;
@property (nonatomic, strong) id<DetailStoryRouterInput> router;

@end
