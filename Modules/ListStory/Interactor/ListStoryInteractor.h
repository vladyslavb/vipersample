//
//  ListStoryInteractor.h
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "ListStoryInteractorInput.h"

// Protocols
@protocol ListStoryInteractorOutput;

@interface ListStoryInteractor : NSObject <ListStoryInteractorInput>

@property (nonatomic, weak) id<ListStoryInteractorOutput> output;

@end
