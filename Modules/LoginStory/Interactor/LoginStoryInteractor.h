//
//  LoginStoryInteractor.h
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "LoginStoryInteractorInput.h"

// Protocols
@protocol LoginStoryInteractorOutput;

@interface LoginStoryInteractor : NSObject <LoginStoryInteractorInput>

@property (nonatomic, weak) id<LoginStoryInteractorOutput> output;

@end
