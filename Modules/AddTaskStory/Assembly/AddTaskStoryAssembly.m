//
//  AddTaskStoryAssembly.m
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "AddTaskStoryAssembly.h"

// Frameworks
#import <ViperMcFlurry/ViperMcFlurry.h>

// Classes
#import "AddTaskStoryViewController.h"
#import "AddTaskStoryInteractor.h"
#import "AddTaskStoryPresenter.h"
#import "AddTaskStoryRouter.h"


@implementation AddTaskStoryAssembly


#pragma mark - Initialization methods -

- (AddTaskStoryViewController*) viewAddTaskStory 
{
    return [TyphoonDefinition withClass: [AddTaskStoryViewController class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterAddTaskStory]];
                              [definition injectProperty: @selector(moduleInput)
                                                    with: [self presenterAddTaskStory]];
                                                    
                          }];
}

- (AddTaskStoryInteractor*) interactorAddTaskStory 
{
    return [TyphoonDefinition withClass: [AddTaskStoryInteractor class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterAddTaskStory]];
                                                    
                          }];
}

- (AddTaskStoryPresenter*) presenterAddTaskStory
{
    return [TyphoonDefinition withClass: [AddTaskStoryPresenter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(view)
                                                    with: [self viewAddTaskStory]];
                              [definition injectProperty: @selector(interactor)
                                                    with: [self interactorAddTaskStory]];
                              [definition injectProperty: @selector(router)
                                                    with: [self routerAddTaskStory]];
                                                    
                          }];
}

- (AddTaskStoryRouter*) routerAddTaskStory
{
    return [TyphoonDefinition withClass: [AddTaskStoryRouter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(transitionHandler)
                                                    with: [self viewAddTaskStory]];
                                                    
                          }];
}

@end
