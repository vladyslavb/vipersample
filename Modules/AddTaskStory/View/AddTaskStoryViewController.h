//
//  AddTaskStoryViewController.h
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
@import UIKit;

// Classes
#import "AddTaskStoryViewInput.h"

// Protocols
@protocol AddTaskStoryViewOutput;

@interface AddTaskStoryViewController : UIViewController <AddTaskStoryViewInput>

@property (nonatomic, strong) id<AddTaskStoryViewOutput> output;

@end
