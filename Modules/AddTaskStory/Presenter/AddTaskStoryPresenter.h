//
//  AddTaskStoryPresenter.h
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "AddTaskStoryViewOutput.h"
#import "AddTaskStoryInteractorOutput.h"
#import "AddTaskStoryModuleInput.h"

// Protocols
@protocol AddTaskStoryViewInput;
@protocol AddTaskStoryInteractorInput;
@protocol AddTaskStoryRouterInput;

@interface AddTaskStoryPresenter : NSObject <AddTaskStoryModuleInput, AddTaskStoryViewOutput, AddTaskStoryInteractorOutput>

@property (nonatomic, weak) id<AddTaskStoryViewInput> view;
@property (nonatomic, strong) id<AddTaskStoryInteractorInput> interactor;
@property (nonatomic, strong) id<AddTaskStoryRouterInput> router;

@end
