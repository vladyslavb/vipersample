//
//  DetailStoryInteractor.h
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "DetailStoryInteractorInput.h"

// Protocols
@protocol DetailStoryInteractorOutput;

@interface DetailStoryInteractor : NSObject <DetailStoryInteractorInput>

@property (nonatomic, weak) id<DetailStoryInteractorOutput> output;

@end
