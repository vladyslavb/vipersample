//
//  LoginStoryPresenter.h
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "LoginStoryViewOutput.h"
#import "LoginStoryInteractorOutput.h"
#import "LoginStoryModuleInput.h"

// Protocols
@protocol LoginStoryViewInput;
@protocol LoginStoryInteractorInput;
@protocol LoginStoryRouterInput;

@interface LoginStoryPresenter : NSObject <LoginStoryModuleInput, LoginStoryViewOutput, LoginStoryInteractorOutput>

@property (nonatomic, weak) id<LoginStoryViewInput> view;
@property (nonatomic, strong) id<LoginStoryInteractorInput> interactor;
@property (nonatomic, strong) id<LoginStoryRouterInput> router;

@end
