//
//  ListStoryPresenter.m
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "ListStoryPresenter.h"

// Classes
#import "ListStoryViewInput.h"
#import "ListStoryInteractorInput.h"
#import "ListStoryRouterInput.h"

@implementation ListStoryPresenter


#pragma mark - Methods ListStoryModuleInput -

- (void) configureModule 
{
	/**
	@author Vladyslav Bedro
	
	Starting configuration of the module, which dependent to the view state
	*/
}


#pragma mark - Methods ListStoryViewOutput -

- (void) didTriggerViewReadyEvent 
{
	[self.view setupInitialState];
}


#pragma mark - Methods ListStoryInteractorOutput -


@end
