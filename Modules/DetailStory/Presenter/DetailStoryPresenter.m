//
//  DetailStoryPresenter.m
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "DetailStoryPresenter.h"

// Classes
#import "DetailStoryViewInput.h"
#import "DetailStoryInteractorInput.h"
#import "DetailStoryRouterInput.h"

@implementation DetailStoryPresenter


#pragma mark - Methods DetailStoryModuleInput -

- (void) configureModule 
{
	/**
	@author Vladyslav Bedro
	
	Starting configuration of the module, which dependent to the view state
	*/
}


#pragma mark - Methods DetailStoryViewOutput -

- (void) didTriggerViewReadyEvent 
{
	[self.view setupInitialState];
}


#pragma mark - Methods DetailStoryInteractorOutput -


@end
