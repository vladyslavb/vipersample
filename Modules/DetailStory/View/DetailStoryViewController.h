//
//  DetailStoryViewController.h
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
@import UIKit;

// Classes
#import "DetailStoryViewInput.h"

// Protocols
@protocol DetailStoryViewOutput;

@interface DetailStoryViewController : UIViewController <DetailStoryViewInput>

@property (nonatomic, strong) id<DetailStoryViewOutput> output;

@end
