//
//  LoginStoryAssembly.m
//  ViperSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "LoginStoryAssembly.h"

// Frameworks
#import <ViperMcFlurry/ViperMcFlurry.h>

// Classes
#import "LoginStoryViewController.h"
#import "LoginStoryInteractor.h"
#import "LoginStoryPresenter.h"
#import "LoginStoryRouter.h"


@implementation LoginStoryAssembly


#pragma mark - Initialization methods -

- (LoginStoryViewController*) viewLoginStory 
{
    return [TyphoonDefinition withClass: [LoginStoryViewController class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterLoginStory]];
                              [definition injectProperty: @selector(moduleInput)
                                                    with: [self presenterLoginStory]];
                                                    
                          }];
}

- (LoginStoryInteractor*) interactorLoginStory 
{
    return [TyphoonDefinition withClass: [LoginStoryInteractor class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterLoginStory]];
                                                    
                          }];
}

- (LoginStoryPresenter*) presenterLoginStory
{
    return [TyphoonDefinition withClass: [LoginStoryPresenter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(view)
                                                    with: [self viewLoginStory]];
                              [definition injectProperty: @selector(interactor)
                                                    with: [self interactorLoginStory]];
                              [definition injectProperty: @selector(router)
                                                    with: [self routerLoginStory]];
                                                    
                          }];
}

- (LoginStoryRouter*) routerLoginStory
{
    return [TyphoonDefinition withClass: [LoginStoryRouter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(transitionHandler)
                                                    with: [self viewLoginStory]];
                                                    
                          }];
}

@end
